﻿using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Softray.Academy.Common;
using Softray.Academy.Repository;
using Softray.Academy.Repository.Interfaces;
using Swashbuckle.AspNetCore.Swagger;

namespace Softray.Academy.Api
{
    /// <summary>
    /// The Startup class of the API, used to configure service and the request pipeline.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">A <see cref="IConfiguration"/> instance.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Gets the Configuration instance.
        /// </summary>
        /// <value>
        /// The Configuration instance.
        /// </value>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method is used to configure services the app is going to use and register them with the DI container.
        /// </summary>
        /// <param name="services">A <see cref="IServiceCollection"/> instance used to register or remove services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => {
                options.Filters.Add(new AuthorizeFilter());
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Softray Academy API",
                    Version = "v1",
                });
            });

            services.AddSingleton<MongoDatabaseBuilder>();
            services.AddSingleton(sp => sp.GetRequiredService<MongoDatabaseBuilder>().Build());
                
            services.AddSingleton<ICourseRepository, CoursesRepository>();
        }

        /// <summary>
        /// This method is used to configure the request pipeline.
        /// </summary>
        /// <param name="app">A <see cref="IApplicationBuilder"/> instance.</param>
        /// <param name="env">A <see cref="IHostingEnvironment"/> instance.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();
            app.UseCors(cors => cors.AllowAnyHeader()   
                                    .AllowAnyMethod()
                                    .AllowAnyOrigin());

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "IDM API V1");
                c.RoutePrefix = string.Empty;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
