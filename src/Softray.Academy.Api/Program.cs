﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Softray.Academy.Api
{
    /// <summary>
    /// Represents the starting Program class of the application.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Represents the application's entry method.
        /// </summary>
        /// <param name="args">An array that contains application arguments.</param>
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Represents the methods which creates the application's Host Builder.
        /// </summary>
        /// <param name="args">Appplication arguments.</param>
        /// <returns>Host Builder.</returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
