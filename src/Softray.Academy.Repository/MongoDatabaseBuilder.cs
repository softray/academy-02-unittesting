﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace Softray.Academy.Common
{
    /// <summary>
    /// Represents a helper class for MongoDatabase creation.
    /// </summary>
    public class MongoDatabaseBuilder
    {
        private readonly DatabaseSettings _databaseSettings = new DatabaseSettings();

        /// <summary>
        /// Initializes a new instance of the <see cref="MongoDatabaseBuilder"/> class.
        /// </summary>
        /// <param name="configuration">Application configuration.</param>
        public MongoDatabaseBuilder(IConfiguration configuration)
        {
            configuration.Bind(nameof(DatabaseSettings), _databaseSettings);
        }

        /// <summary>
        /// Builds a <see cref="IMongoDatabase"/> instance using the provided database settings.
        /// </summary>
        /// <returns>A <see cref="IMongoDatabase"/> instance.</returns>
        public IMongoDatabase Build()
        {
            IMongoClient client = new MongoClient(_databaseSettings.ConnectionString);
            IMongoDatabase database = client.GetDatabase(
                _databaseSettings.DatabaseName,
                CreateDatabaseSettings());

            return database;
        }

        private MongoDatabaseSettings CreateDatabaseSettings()
        {
            return new MongoDatabaseSettings { GuidRepresentation = MongoDB.Bson.GuidRepresentation.Standard };
        }
    }
}
