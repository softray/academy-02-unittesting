﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Softray.Academy.Repository.Interfaces
{
    /// <summary>
    /// Represents a generic abstract repository enabling CRUD operations.
    /// </summary>
    /// <typeparam name="TEntity">Type of the entity that will be manipulated using the repository.</typeparam>
    public interface IRepository<TEntity>
    {
        /// <summary>
        /// Returns a list of all objects from the data store.
        /// </summary>
        /// <returns>List of all objects.</returns>
        Task<List<TEntity>> GetAsync();
        
        /// <summary>
        /// Returns the object with the specified Id from the data store.
        /// </summary>
        /// <param name="id">Id of the object to be returned.</param>
        /// <returns>The object that has the specified Id.</returns>
        Task<TEntity> GetAsync(Guid id);

        /// <summary>
        /// Creates an entity inside the data store.
        /// </summary>
        /// <param name="entity">The entity to be created inside the data store.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task CreateAsync(TEntity entity);

        /// <summary>
        /// Updates an entity inside the data store.
        /// </summary>
        /// <param name="entityToUpdate">The updated version of the entity.</param>
        /// <returns>The updated entity.</returns>
        Task<TEntity> UpdateAsync(TEntity entityToUpdate);

        /// <summary>
        /// Removes the entity with the specified Id from the data store.
        /// </summary>
        /// <param name="id">Id of the entity to be removed.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task RemoveAsync(Guid id);
    }
}
