﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Softray.Academy.Common
{
    /// <summary>
    /// Database Settings concrete implementation.
    /// </summary>
    public class DatabaseSettings
    {
        /// <summary>
        /// Gets or sets the Connection String for Mongo DB.
        /// </summary>
        /// <value>
        /// The Connection String for Mongo DB.
        /// </value>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the DatabaseName.
        /// </summary>
        /// <value>
        /// The DatabaseName.
        /// </value>
        public string DatabaseName { get; set; }
    }
}
