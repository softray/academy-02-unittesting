using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Econolite.Idm.Api;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using Econolite.Idm.Entities;
using Econolite.Idm.Repository.Interfaces;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Moq;
using Xunit;

namespace Econolite.Idm.FunctionalTests
{
    #region CustomWebApplicationFactory

    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup>
    {
        public byte[] SecurityVector { get; set; }

        public CustomWebApplicationFactory()
        {
            
            RepositoryMock = new Mock<IDetectorRepository>();
        }

        public Mock<IDetectorRepository> RepositoryMock { get; }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {

            builder.ConfigureServices(services =>
            {
                services.AddAuthentication("test")
                .AddJwtBearer("test", cfg =>
                {
                    cfg.SaveToken = true;
                    cfg.Authority = "https://ident.cosysdev.com";

                    cfg.RequireHttpsMetadata = false;
                    cfg.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = false,
                        IssuerSigningKey = new SymmetricSecurityKey(SecurityVector),
                    };
                });

                services.AddAuthorization(options =>
                {
                    var defaultAuthorizationPolicyBuilder = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme, "test");
                    defaultAuthorizationPolicyBuilder = defaultAuthorizationPolicyBuilder.RequireAuthenticatedUser();
                    options.DefaultPolicy = defaultAuthorizationPolicyBuilder.Build();
                });

                services.Replace(ServiceDescriptor.Singleton<IDetectorRepository>(RepositoryMock.Object));
            });
        }
    }

    #endregion CustomWebApplicationFactory

    public class BasicWebTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly IFixture _fixture = new Fixture();

        private readonly CustomWebApplicationFactory<Startup> _applicationFactory;

        public BasicWebTests(CustomWebApplicationFactory<Startup> factory)
        {
            _applicationFactory = factory;
            _applicationFactory.SecurityVector = Encoding.UTF8.GetBytes(_fixture.CreateMany<char>(16).ToArray());
        }

        [Fact]
        public async Task GetAsyncAuthorizationSuccess()
        {
            // Arrange
            _applicationFactory.RepositoryMock.Setup(_ => _.GetAsync())
                .ReturnsAsync(_fixture.CreateMany<Detector>().ToList());
            var client = _applicationFactory.CreateClient();
            
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", GetBearer(1));

            // Act
            var response = await client.GetAsync("/api/detectors");

            // Assert
            Assert.NotNull(response);
            Assert.True(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task GetAsyncTokenExpired()
        {
            // Arrange
            var client = _applicationFactory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", GetBearer(-1));

            // Act
            var response = await client.GetAsync("/api/detectors");
            
            // Assert
            Assert.NotNull(response);
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Fact]
        public async Task GetAsyncBearerEmpty()
        {
            // Arrange
            var client = _applicationFactory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", string.Empty);

            // Act
            var response = await client.GetAsync("/api/detectors");
            
            // Assert
            Assert.NotNull(response);
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Fact]
        public async Task GetAsyncAuthorizationMissing()
        {
            // Act
            var client = _applicationFactory.CreateClient();
            var response = await client.GetAsync("/api/detectors");

            // Assert
            Assert.NotNull(response);
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        #region Private Methods

        private string GetBearer(int days)
        {
            var key = new SymmetricSecurityKey(_applicationFactory.SecurityVector);
            var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

            var token = new JwtSecurityToken(
                "https://ident.cosysdev.com",
                "https://ident.cosysdev.com/resources",
                new Claim[] { },
                expires: DateTime.Now.AddDays(days),
                signingCredentials: signingCredentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        #endregion Private Methods
    }
}
