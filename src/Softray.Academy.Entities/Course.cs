﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace Softray.Academy.Entities
{
    /// <summary>
    /// Represents the Course model.
    /// </summary>
    public class Course : EntityBase
    {
        /// <summary>
        /// Gets or sets the name of the course.
        /// </summary>
        /// <value>
        /// The name of the course.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of students.
        /// </summary>
        /// <value>
        /// The maximum number of students.
        /// </value>
        public int MaxNumberOfStudents { get; set; }

        /// <summary>
        /// Gets or sets the course duration in hours.
        /// </summary>
        /// <value>
        /// The course duration in hours.
        /// </value>
        public int DurationInHours { get; set; }

        /// <summary>
        /// Gets or sets the description of the course.
        /// </summary>
        /// <value>
        /// The description of the course.
        /// </value>
        public string Description { get; set; }
    }
}
